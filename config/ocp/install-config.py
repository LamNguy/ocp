with open('install-config.yaml', 'r', encoding='utf-8') as file:
    data = file.readlines()
    with open('pull-secret.js','r') as secret:
       data[23] =  '  ' + secret.read()
    with open('ocp_rsa.pub', 'r') as key:
       data[25] = '  ' + key.read()
with open('install-config.yaml', 'w', encoding='utf-8') as file:
    file.writelines(data)