# ------------------------------------------------------------------------------
# Private container registry
# ------------------------------------------------------------------------------
export PRIVATE_REGISTRY_LOCATION=registry.ocp4.svtech.gay:8443
export PRIVATE_REGISTRY_PUSH_USER=init
export PRIVATE_REGISTRY_PUSH_PASSWORD=admin123
export PRIVATE_REGISTRY_PULL_USER=init
export PRIVATE_REGISTRY_PULL_PASSWORD=admin123

# for openshift connection
#export OCP_URL=https://apps.ocp4.example.com:6443

export OCP_URL=https://api.ocp4.svtech.gay:6443
export OPENSHIFT_TYPE=self-managed
export IMAGE_ARCH=amd64
export OCP_USERNAME=kubeadmin
export OCP_PASSWORD=fkE4V-Uijey-vHSUC-ijizf
#export OCP_TOKEN=sha256~sxxc1ZrziADqKGtq6B4peAJUh0JwRygiE1zL2dVRtk4


# ------------------------------------------------------------------------------
# Cloud Pak for Data version
# ------------------------------------------------------------------------------
export PROJECT_CPFS_OPS=ibm-common-services
export PROJECT_CPD_OPS=ibm-common-services
export PROJECT_CATSRC=openshift-marketplace
export PROJECT_CPD_INSTANCE=cpd-instance
#export PROJECT_TETHERED=cpd-instance-tether


export STG_CLASS_BLOCK=managed-nfs-storage
export STG_CLASS_FILE=managed-nfs-storage

# ------------------------------------------------------------------------------
# IBM Entitled Registry
# ------------------------------------------------------------------------------
export IBM_ENTITLEMENT_KEY=



# ------------------------------------------------------------------------------
# Components
# ------------------------------------------------------------------------------
export COMPONENTS=cpfs,cpd_platform,wkc
export VERSION=4.6.6

# ------------------------------------------------------------------------------
# NFS storage
# ------------------------------------------------------------------------------

export OLM_UTILS_IMAGE=${PRIVATE_REGISTRY_LOCATION}/olm-utils:latest
export NFS_SERVER_LOCATION=192.168.30.
export NFS_PATH=/exports
export PROJECT_NFS_PROVISIONER=nfs-provisioner
export NFS_STORAGE_CLASS=managed-nfs-storage
export NFS_IMAGE=k8s.gcr.io/sig-storage/nfs-subdir-external-provisioner:v4.0.2